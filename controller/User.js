const jwt = require("jsonwebtoken");
const passport = require("passport");
const UserModel = require("../model/User")


module.exports.signup = async (req, res, next) => {
    const body = { _id: req.user._id, email: req.user.email };
    const token = jwt.sign({ user: body }, process.env.APP_SECRET);
    res.json({
        message: "Signup successful",
        user: req.user,
        token: token
    });
}


module.exports.login = async (req, res, next) => {
    passport.authenticate(
        "login",
        async (err, user) => {
            try {
                if (err || !user) {
                    const error = new Error("An error occurred.");
                    return next(error);
                }
                req.login(
                    user,
                    { session: false },
                    async (error) => {
                        if (error) return next(error);

                        const body = { _id: user._id, email: user.email };
                        const token = jwt.sign({ user: body }, process.env.APP_SECRET);

                        return res.json({ token, _id: user._id });
                    }
                );
            } catch (error) {
                return next(error);
            }
        }
    )(req, res, next);
}

module.exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await UserModel.find()
        res.json(users)
    }
    catch (err) {
        next(err)
    }
}

module.exports.getProfile = async (req, res, next) => {
    try {
        const user = await UserModel.findOne({ _id: req.params.id })
        if (!user) {
            const error = new Error("User not found.");
            return next(error);
        }
        res.json(user)
    }
    catch (err) {
        next(err)
    }
}

module.exports.updateProfile = async (req, res, next) => {
    try {
        const updateResp = await UserModel.updateOne({ _id: req.params.id }, { $set: { ...req.body } })
        res.json({ updated: updateResp.nModified > 0 })
    }
    catch (err) {
        next(err)
    }

}