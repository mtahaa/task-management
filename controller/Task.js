const UserModel = require("../model/User")
const TaskModel = require("../model/Task")


module.exports.getAllTasks = async (req, res, next) => {

    try {
        const tasks = await TaskModel.find({})
        res.json(tasks)
    } catch (err) {
        next(err)
    }
}

module.exports.createNewTask = async (req, res, next) => {
    try {
        const loggedInUser = await UserModel.findOne({ _id: req.user._id })
        if (loggedInUser.role !== 'admin') {
            return next(new Error("Access unauthorized, users are not allowed to create new tasks."))
        }
        const taskCreationResp = await TaskModel.create({ ...req.body, createdBy: loggedInUser._id })
        res.json({ created: !!taskCreationResp._id, _id: taskCreationResp._id })
    } catch (err) {
        console.log("Unable to create new task ", err)
        next(err)
    }

}

module.exports.getTaskById = async (req, res, next) => {
    try {
        const task = await TaskModel.findOne({ _id: req.params.id })
        res.json(task)
    } catch (err) {
        next(err)
    }
}

module.exports.getLoggedInUsersTasks = async (req, res, next) => {
    try {
        const loggedInUser = await UserModel.findOne({ _id: req.user._id })
        if (!loggedInUser) {
            return next(new Error("Invalid User. Please login again"))
        }
        // TODO: use constants service for user roles instead of hardcoded string
        const query = loggedInUser.role === 'admin' ? { createdBy: loggedInUser._id } : { workerId: loggedInUser._id }
        const tasks = await TaskModel.find(query)
        res.json(tasks)
    } catch (err) {
        console.log("Unable to get task of logged in user", err)
        next(err)
    }
}

module.exports.updateTask = async (req, res, next) => {

    const taskId = req.params.id
    const { action, workerId } = req.body
    if (!taskId) {
        return next(new Error("Invalid params received. Please verify the task Id"))
    }
    try {
        let updateResp;
        switch (action) {
            case "assign_worker":
                if (!workerId) {
                    return next(new Error("Invalid params received. Please verify the worker Id"))
                }
                updateResp = await TaskModel.updateOne(
                    // An admin can assign task to any other user (or worker). 
                    // at the time of assignment, the task must be unassigned by default
                    { _id: taskId, createdBy: req.user._id, status: "unassigned" },
                    { $set: { workerId, status: "assigned" } }
                )
                break
            case "mark_as_completed":
                updateResp = await TaskModel.updateOne(
                    // A User (worker) can mark the task as completed
                    // at the time of completion, the task must be assigned to logged in user
                    { _id: taskId, status: "assigned", workerId: req.user._id },
                    { $set: { status: "completed", completedOn: new Date() } }
                )
                break
            default:
                return next(new Error("Invalid params received. Please verify the action"))
        }

        res.json({ updated: updateResp.nModified > 0 })
    } catch (err) {
        next(err)
    }
}

