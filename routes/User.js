const express = require("express");
const userController = require("../controller/User")
const passport = require("passport");
const router = express.Router();


router.post("/signup", passport.authenticate("signup", { session: false }), userController.signup);
router.post("/login", userController.login);
router.get("/", passport.authenticate("jwt", { session: false }), userController.getAllUsers);
router.get("/profile/:id", passport.authenticate("jwt", { session: false }), userController.getProfile);
router.patch("/profile/:id", passport.authenticate("jwt", { session: false }), userController.updateProfile);


module.exports = router;