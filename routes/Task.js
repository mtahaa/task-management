const express = require("express");
const router = express.Router();
const taskController = require("../controller/Task");


router.get("/", taskController.getAllTasks);
router.post("/", taskController.createNewTask);
router.get("/mine", taskController.getLoggedInUsersTasks);
router.patch("/:id", taskController.updateTask);
router.get("/:id", taskController.getTaskById);

module.exports = router;