const express = require("express");
const bodyParser = require("body-parser");
const passport = require("passport");
const dotenv = require("dotenv")
const dbHelper = require("./model/dbHelper")
const authHelper = require("./auth/auth")

dotenv.config()
const userRoutes = require("./routes/User");
const taskRoutes = require("./routes/Task");

dbHelper.init()
authHelper.init()
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/users", userRoutes);
app.use("/tasks", passport.authenticate("jwt", { session: false }), taskRoutes);


app.get("*", (req, res) => {
    res.json({ status: false, message: "Unhandled route" })
})
// Handle errors.
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({ error: err });
});

app.listen(process.env.PORT || 3000, () => {
    console.log("Server started.")
});