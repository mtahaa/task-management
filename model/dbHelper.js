const mongoose = require('mongoose');

module.exports.init = function () {

    mongoose.connect(process.env.MONGO_DB_URI, { useUnifiedTopology: true});
    mongoose.connection.on('error', error => console.log("Unable to connect with DB", error));
    mongoose.Promise = global.Promise;
}