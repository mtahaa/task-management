const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = new Schema({
    createdOn: {
        type: Date,
        default: Date.now()
    },
    completedOn: Date,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    workerId: mongoose.Schema.Types.ObjectId,
    title: {
        type: String,
        required: true
    },
    description : String,
    status: {
        type: String,
        default: "unassigned"
    } // unassigned, assigned, completed
});

const TaskModel = mongoose.model('tasks', TaskSchema);

module.exports = TaskModel;